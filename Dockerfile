FROM node:14

WORKDIR /joan/src/app

COPY . .

RUN npm install

RUN npm run build

EXPOSE 3000
CMD ["node", "dist/main"]