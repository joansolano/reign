import { HighlightResultEntity } from "../../../highlight_result/infrastructure/highlight-result.entity";

export interface MatchedWordsProperties {
    readonly id: number,
    readonly word: string,
    readonly id_highlight: HighlightResultEntity
}

export class MatchedWords {
    public id: number;
    public word: string;
    public id_highlight: HighlightResultEntity;

    constructor (properties: MatchedWordsProperties) {
        this.id = properties.id,
        this.word = properties.word,
        this.id_highlight = properties.id_highlight
    }

    properties(): MatchedWordsProperties {
        return {
            id: this.id,
            word: this.word,
            id_highlight: this.id_highlight
        }
    }
}