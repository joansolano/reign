import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { MatchedWordsEntity } from "../infrastructure/matched-words.entity";
import { MatchedWords } from "../domain/aggregates/matched-words.model"
import { HighlightResultEntity } from "../../highlight_result/infrastructure/highlight-result.entity";

@Injectable()
export class MatchedWordsService {

    constructor (
        @InjectRepository(MatchedWordsEntity)
        private matchedWordsRepository: Repository<MatchedWordsEntity>
    ) {}

    async createWord(
        word: string,
        id_highlight: HighlightResultEntity
    ): Promise<void> {
        const wordModel = new MatchedWords({
            id: await this.newId(),
            word,
            id_highlight
        });

        await this.matchedWordsRepository.save(wordModel);
    }

    private async newId(): Promise<number> {
        const emptyWord = new MatchedWordsEntity();
        emptyWord.word = '';
        emptyWord.id_highlight = null;

        const word = await this.matchedWordsRepository.save(emptyWord);
        return word.id;
    }

}