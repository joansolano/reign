import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MatchedWordsService } from "../application/matched-words.service";
import { MatchedWordsEntity } from "./matched-words.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            MatchedWordsEntity
        ])
    ],
    providers: [
        MatchedWordsService
    ],
    exports: [
        MatchedWordsService
    ]
})
export class MatchedWordsModule {}