import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultEntity } from "../../highlight_result/infrastructure/highlight-result.entity";

@Entity('matchedwords')
export class MatchedWordsEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column("varchar")
    word!: string;

    @ManyToOne(type => HighlightResultEntity, highlight => highlight.matchedWords)
    @JoinColumn({name: 'id_highlight'})
    id_highlight!: HighlightResultEntity;

}