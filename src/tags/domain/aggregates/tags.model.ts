import { ArticlesEntity } from "../../../articles/infrastructure/articles.entity";

export interface TagsProperties {
    readonly id: number,
    readonly tag: string,
    readonly id_article: ArticlesEntity
}

export class Tags {
    public id: number;
    public tag: string;
    public id_article: ArticlesEntity;

    constructor (properties: TagsProperties) {
        this.id = properties.id,
        this.tag = properties.tag,
        this.id_article = properties.id_article
    }

    properties(): TagsProperties {
        return {
            id: this.id,
            tag: this.tag,
            id_article: this.id_article
        }
    }
}