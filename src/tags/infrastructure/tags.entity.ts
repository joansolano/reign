import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ArticlesEntity } from "../../articles/infrastructure/articles.entity";

@Entity('tags')
export class TagsEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column("varchar")
    tag!: string;

    @ManyToOne(type => ArticlesEntity, article => article._tags)
    @JoinColumn({name: 'id_article'})
    id_article!: ArticlesEntity;

}