import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TagsService } from "../application/tags.service";
import { TagsEntity } from "./tags.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            TagsEntity
        ])
    ],
    providers: [
        TagsService
    ],
    exports: [
        TagsService
    ]
})
export class TagsModule {}