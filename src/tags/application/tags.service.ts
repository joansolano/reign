import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ArticlesEntity } from "../../articles/infrastructure/articles.entity";
import { TagsEntity } from "../infrastructure/tags.entity";
import { Tags } from "../domain/aggregates/tags.model"

@Injectable()
export class TagsService {

    constructor (
        @InjectRepository(TagsEntity)
        private tagsRepository: Repository<TagsEntity>
    ) {}

    async createTag(
        tag: string,
        id_article: ArticlesEntity
    ): Promise<void> {
        const tagModel = new Tags({
            id: await this.newId(),
            tag,
            id_article
        });

        await this.tagsRepository.save(tagModel);
    }

    private async newId(): Promise<number> {
        const emptyTag = new TagsEntity();
        emptyTag.tag = '';
        emptyTag.id_article = null;

        const tag = await this.tagsRepository.save(emptyTag);
        return tag.id;
    }

}