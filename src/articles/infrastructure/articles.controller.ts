import { Controller, Get, Param, Put, Query } from "@nestjs/common";
import { ArticlesService } from "../application/articles.service";
import { QueryArticlesDTO } from "./queries/query.dto";

@Controller('articles')
export class ArticlesController {

    constructor (
        private articlesService: ArticlesService
    ) {}

    @Get()
    async getAllArticles(@Query() query: QueryArticlesDTO): Promise<Object> {
        try {
            const data = await this.articlesService.findPaginatedArticles(
                Number(query.articles),
                Number(query.page),
                query.author,
                query.title
            );
            const newData = [];

            for (let i = 0; i < data.length; i++) {
                newData.push({
                    ...data[i],
                    _tags: this.articlesService.extractTags(data[i]._tags),
                    _highlightResults: this.articlesService.extractHighlightResults(data[i]._highlightResults)
                })
            }
            
            return {
                data: newData,
                error: null
            }
        } catch (error) {
            return {
                data: null,
                error: error
            }
        }
    }

    @Put(':id')
    async updateVisibilityArticle(@Param() id: string): Promise<Object> {
        try {
            await this.articlesService.removeArticle(Number(id['id']));
            return {
                data: 'Article removed',
                error: null
            }
        } catch (error) {
            return {
                data: null,
                error: error
            }
        }
    }

}