import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultEntity } from "../../highlight_result/infrastructure/highlight-result.entity";
import { TagsEntity } from "../../tags/infrastructure/tags.entity";

@Entity('articles')
export class ArticlesEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        type: "timestamp without time zone",
        nullable: false
    })
    created_at!: Date;

    @Column({
        type: "varchar",
        nullable: true
    })
    title!: string | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    url!: string | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    author!: string | null;

    @Column({
        type: "int",
        nullable: true
    })
    points!: number | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    story_text!: string | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    comment_text!: string | null;

    @Column({
        type: "int",
        nullable: true
    })
    num_comments!: number | null;

    @Column({
        type: "int",
        nullable: true
    })
    story_id!: number | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    story_title!: string | null;

    @Column({
        type: "varchar",
        nullable: true
    })
    story_url!: string | null;

    @Column({
        type: "int",
        nullable: true
    })
    parent_id!: number | null;

    @Column({
        type: "bigint",
        nullable: true
    })
    created_at_i!: number | null;

    @OneToMany(type => TagsEntity, tags => tags.id_article, {cascade: true})
    _tags!: TagsEntity[];

    @Column({
        type: "varchar",
        nullable: true
    })
    objectID!: string | null;

    @OneToMany(type => HighlightResultEntity, highlight => highlight.article, {cascade: true})
    _highlightResults!: HighlightResultEntity[];

    @Column("boolean")
    is_visible!: boolean;

}