import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class QueryArticlesDTO {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly page: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly articles: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly author: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    readonly title: string;

}