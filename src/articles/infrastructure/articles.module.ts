import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { HighlightResultModule } from "../../highlight_result/infrastructure/highlight-result.module";
import { TagsModule } from "../../tags/infrastructure/tags.module";
import { ArticlesService } from "../application/articles.service";
import { ArticlesController } from "./articles.controller";
import { ArticlesEntity } from "./articles.entity";

@Module({
    imports: [
        HttpModule,
        TypeOrmModule.forFeature([
            ArticlesEntity
        ]),
        TagsModule,
        HighlightResultModule
    ],
    providers: [
        ArticlesService
    ],
    controllers: [
        ArticlesController
    ]
})
export class ArticlesModule {}