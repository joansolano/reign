import { TagsEntity } from "../../../tags/infrastructure/tags.entity";

export interface ArticlesProperties {
    readonly id: number,
    readonly title: string,
    readonly created_at: Date,
    readonly url: string,
    readonly author: string,
    readonly points: number,
    readonly story_text: string,
    readonly comment_text: string,
    readonly num_comments: number,
    readonly story_id: number,
    readonly story_title: string,
    readonly story_url: string,
    readonly parent_id: number,
    readonly created_at_i: number,
    readonly objectID: string,
    readonly is_visible: boolean
}

export class Articles {
    public id: number;
    public title: string;
    public created_at: Date;
    public url: string;
    public author: string;
    public points: number;
    public story_text: string;
    public comment_text: string;
    public num_comments: number;
    public story_id: number;
    public story_title: string;
    public story_url: string;
    public parent_id: number;
    public created_at_i: number;
    public objectID: string;
    public is_visible: boolean;

    constructor (properties: ArticlesProperties) {
        this.id = properties.id,
        this.created_at = properties.created_at,
        this.title = properties.title,
        this.url = properties.url,
        this.author = properties.author,
        this.points = properties.points,
        this.story_text = properties.story_text,
        this.comment_text = properties.comment_text,
        this.num_comments = properties.num_comments,
        this.story_id = properties.story_id,
        this.story_title = properties.story_title,
        this.story_url = properties.story_url,
        this.parent_id = properties.parent_id,
        this.created_at_i = properties.created_at_i,
        this.objectID = properties.objectID,
        this.is_visible = properties.is_visible
    }

    properties(): ArticlesProperties {
        return {
            id: this.id,
            created_at: this.created_at,
            title: this.title,
            url: this.url,
            author: this.author,
            points: this.points,
            story_text: this.story_text,
            comment_text: this.comment_text,
            num_comments: this.num_comments,
            story_id: this.story_id,
            story_title: this.story_title,
            story_url: this.story_url,
            parent_id: this.parent_id,
            created_at_i: this.created_at_i,
            objectID: this.objectID,
            is_visible: this.is_visible
        }
    }

}