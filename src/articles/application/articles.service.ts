import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { InjectRepository } from "@nestjs/typeorm";
import { lastValueFrom, map } from "rxjs";
import { Repository } from "typeorm";
import { HighlightResultService } from "../../highlight_result/application/highlight-result.service";
import { HighlightResultEntity } from "../../highlight_result/infrastructure/highlight-result.entity";
import { MatchedWordsEntity } from "../../matched_words/infrastructure/matched-words.entity";
import { TagsService } from "../../tags/application/tags.service";
import { TagsEntity } from "../../tags/infrastructure/tags.entity";
import { Articles } from "../domain/aggregates/articles.model";
import { ArticlesEntity } from "../infrastructure/articles.entity";

@Injectable()
export class ArticlesService {

    constructor (
        private httpService: HttpService,
        @InjectRepository(ArticlesEntity)
        private articlesRepository: Repository<ArticlesEntity>,
        private tagsService: TagsService,
        private highlightResultService: HighlightResultService
    ) {}

    async findAll(): Promise<ArticlesEntity[]> {
        return await this.articlesRepository.find({
            take: 1
        });
    }

    async findPaginatedArticles(
        limit: number,
        offset: number,
        author: string,
        story_title: string
    ): Promise<ArticlesEntity[]> {
        const whereArray = [];

        if (author) whereArray.push({author, is_visible: true});
        if (story_title) whereArray.push({story_title, is_visible: true});

        return await this.articlesRepository.find({
            where: whereArray,
            relations: [
                '_tags',
                '_highlightResults',
                '_highlightResults.matchedWords'
            ],
            skip: (offset - 1) * 5,
            take: limit
        });
    }

    async createArticle(
        created_at: Date,
        title: string,
        url: string,
        author: string,
        points: number,
        story_text: string,
        comment_text: string,
        num_comments: number,
        story_id: number,
        story_title: string,
        story_url: string,
        parent_id: number,
        created_at_i: number,
        objectID: string
    ): Promise<ArticlesEntity> {
        const articlesModel = new Articles({
            id: await this.newId(),
            created_at,
            title,
            url,
            author,
            points,
            story_text,
            comment_text,
            num_comments,
            story_id,
            story_title,
            story_url,
            parent_id,
            created_at_i,
            objectID,
            is_visible: true
        });

        return await this.articlesRepository.save(articlesModel);
    }

    async removeArticle(id: number): Promise<void> {
        const articleToRemove = await this.articlesRepository.findOne(id);
        articleToRemove.is_visible = false;

        await this.articlesRepository.save(articleToRemove);
    }

    async triggerArticleCreation(): Promise<void> {
        const fetchedData = await this.getDataFromHackerNews();
        const hits = fetchedData['hits'];

        for (let i = 0; i < hits.length; i++) {
            const isArticle = await this.articlesRepository.findOne({
                story_id: hits[i]['story_id']
            })
    
            if (!isArticle) {
                const articleCreated = await this.createArticle(
                    new Date(hits[i]['created_at']),
                    hits[i]['title'],
                    hits[i]['url'],
                    hits[i]['author'],
                    hits[i]['points'],
                    hits[i]['story_text'],
                    hits[i]['comment_text'],
                    hits[i]['num_comments'],
                    hits[i]['story_id'],
                    hits[i]['story_title'],
                    hits[i]['story_url'],
                    hits[i]['parent_id'],
                    hits[i]['created_at_i'],
                    hits[i]['objectID'],
                );
        
                for (let j = 0; j < hits[i]['_tags'].length; j++) {
                    await this.tagsService.createTag(
                        hits[i]['_tags'][j],
                        articleCreated
                    )
                }
        
                const highlightResult = hits[i]['_highlightResult'];
                const highlightResultKeys = Object.keys(highlightResult);

                for (let j = 0; j < highlightResultKeys.length; j++) {
                    await this.highlightResultService.create(
                        highlightResult[highlightResultKeys[j]]['value'],
                        highlightResult[highlightResultKeys[j]]['matchLevel'],
                        highlightResult[highlightResultKeys[j]]['fullyHighlighted'] !== undefined
                            ? highlightResult[highlightResultKeys[j]]['fullyHighlighted']
                            : null,
                        highlightResultKeys[j],
                        articleCreated,
                        highlightResult[highlightResultKeys[j]]['matchedWords']
                    );
                }
            }
        }
    }

    @Cron(new Date(Date.now() + 5 * 1000))
    async fetchDataAtTheBeggining(): Promise<void> {
        const isData = await this.findAll();
        if (!isData.length) {
            await this.triggerArticleCreation();
            console.log('Fetching data at the beggining')
        }
    }

    @Cron(CronExpression.EVERY_HOUR)
    async fetchDataEveryMinute(): Promise<void> {
        await this.triggerArticleCreation();
        console.log('Fetching data every hour')
    }

    private async newId(): Promise<number> {
        const emptyArticle = new ArticlesEntity();
        emptyArticle.created_at = new Date();
        emptyArticle.title = null;
        emptyArticle.url = null;
        emptyArticle.author = null;
        emptyArticle.points = null;
        emptyArticle.story_text = null;
        emptyArticle.comment_text = null;
        emptyArticle.num_comments = null;
        emptyArticle.story_id = null;
        emptyArticle.story_title = null;
        emptyArticle.story_url = null;
        emptyArticle.parent_id = null;
        emptyArticle.created_at_i = null;
        emptyArticle.objectID = null;
        emptyArticle.is_visible = true;

        const article = await this.articlesRepository.save(emptyArticle);
        return article.id;
    }

    private async getDataFromHackerNews(): Promise<Object> {
        const data = await lastValueFrom(this.httpService.get(
                'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
            ).pipe(map(response => response.data)));
        return data;
    }

    public extractTags(tags: TagsEntity[]): string[] {
        const newTags: string[] = [];

        for (let i = 0; i < tags.length; i++) {
            newTags.push(tags[i].tag);
        }

        return newTags;
    }

    private extractWords(words: MatchedWordsEntity[]): string[] {
        const newWords: string[] = [];

        for (let i = 0; i < words.length; i++) {
            newWords.push(words[i].word);
        }

        return newWords;
    }

    public extractHighlightResults(hlResults: HighlightResultEntity[]): Object {
        const newHLResults: { [key: string]: Object } = {};

        for (let i = 0; i < hlResults.length;  i++) {
            if (hlResults[i].fullyHighlighted !== null) {
                newHLResults[hlResults[i].type] = {
                    value: hlResults[i].value,
                    matchLevel: hlResults[i].matchLevel,
                    fullyHighlighted: hlResults[i].fullyHighlighted,
                    matchedWords: this.extractWords(hlResults[i].matchedWords)
                }
            } else {
                newHLResults[hlResults[i].type] = {
                    value: hlResults[i].value,
                    matchLevel: hlResults[i].matchLevel,
                    matchedWords: this.extractWords(hlResults[i].matchedWords)
                }
            }
        }

        return newHLResults;
    }

}