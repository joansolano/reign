import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ArticlesEntity } from "../../articles/infrastructure/articles.entity";
import { MatchedWordsService } from "../../matched_words/application/matched-words.service";
import { HighlightResult } from "../domain/aggregates/highlight-result.model";
import { HighlightResultEntity } from "../infrastructure/highlight-result.entity";

@Injectable()
export class HighlightResultService {

    constructor (
        @InjectRepository(HighlightResultEntity)
        private hightlightResultRepository: Repository<HighlightResultEntity>,
        private matchedWordsService: MatchedWordsService
    ) {}

    async create(
        value: string,
        matchLevel: string,
        fullyHighlighted: boolean,
        type: string,
        article: ArticlesEntity,
        matchedWords: string[]
    ): Promise<void> {
        const HighlightResultModel = new HighlightResult({
            id: await this.newId(),
            value,
            matchLevel,
            fullyHighlighted,
            type,
            article
        });

        const highlightResultCreated = 
            await this.hightlightResultRepository.save(HighlightResultModel);

        for (let i = 0; i < matchedWords.length; i++) {
            await this.matchedWordsService.createWord(
                matchedWords[i],
                highlightResultCreated
            )
        }
    }

    private async newId(): Promise<number> {
        const emptyHighlightResult = new HighlightResultEntity();
        emptyHighlightResult.value = '';
        emptyHighlightResult.matchLevel = '';
        emptyHighlightResult.fullyHighlighted = false;
        emptyHighlightResult.type = '';

        const highlightresult = await this.hightlightResultRepository.save(emptyHighlightResult);
        return highlightresult.id;
    }

}