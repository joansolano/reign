import { ArticlesEntity } from "../../../articles/infrastructure/articles.entity";

export interface HighlightResultProperties {
    readonly id: number,
    readonly value: string,
    readonly matchLevel: string,
    readonly fullyHighlighted: boolean,
    readonly type: string,
    readonly article: ArticlesEntity
}

export class HighlightResult {
    public id: number;
    public value: string;
    public matchLevel: string;
    public fullyHighlighted: boolean;
    public type: string;
    public article: ArticlesEntity

    constructor (properties: HighlightResultProperties) {
        this.id = properties.id,
        this.value = properties.value,
        this.matchLevel = properties.matchLevel,
        this.fullyHighlighted = properties.fullyHighlighted,
        this.type = properties.type,
        this.article = properties.article
    }

    properties(): HighlightResultProperties {
        return {
            id: this.id,
            value: this.value,
            matchLevel: this.matchLevel,
            fullyHighlighted: this.fullyHighlighted,
            type: this.type,
            article: this.article
        }
    }

}