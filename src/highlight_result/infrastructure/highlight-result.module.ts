import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MatchedWordsModule } from "../../matched_words/infrastructure/matched-words.module";
import { HighlightResultService } from "../application/highlight-result.service";
import { HighlightResultEntity } from "./highlight-result.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            HighlightResultEntity
        ]),
        MatchedWordsModule
    ],
    providers: [
        HighlightResultService
    ],
    exports: [
        HighlightResultService
    ]
})
export class HighlightResultModule {}