import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ArticlesEntity } from "../../articles/infrastructure/articles.entity";
import { MatchedWordsEntity } from "../../matched_words/infrastructure/matched-words.entity";

@Entity('highlightresult')
export class HighlightResultEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column("varchar")
    value!: string;
    
    @Column("varchar")
    matchLevel!: string;

    @Column({
        type: "boolean",
        nullable: true
    })
    fullyHighlighted!: boolean | null;

    @Column("varchar")
    type!: string;

    @OneToMany(type => MatchedWordsEntity, word => word.id_highlight, {cascade: true})
    matchedWords!: MatchedWordsEntity[];

    @ManyToOne(type => ArticlesEntity, article => article._highlightResults)
    @JoinColumn({name: 'article'})
    article!: ArticlesEntity;

}