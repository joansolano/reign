import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/infrastructure/articles.module';
import { CoreModule } from './core.module';
import { HighlightResultModule } from './highlight_result/infrastructure/highlight-result.module';
import { MatchedWordsModule } from './matched_words/infrastructure/matched-words.module';
import { TagsModule } from './tags/infrastructure/tags.module';

@Module({
  imports: [
    CoreModule,
    ArticlesModule,
    TagsModule,
    HighlightResultModule,
    MatchedWordsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
